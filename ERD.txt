--------------------------------------------
Entitas :   Account
Atribut :   account_id (primary key)
            username (UNIQUE)
            password
relasi  :   one to one dengan Profile
            one to many dengan Post     
--------------------------------------------   
Entitas :   Profile
Atribut :   profile_id (primary key)
            username (Foreign key)
            nickname
            description
relasi  :   one to one dengan Account  
--------------------------------------------  
Entitas :   Post
Atribut :   post_id (primary key)
            username (Foreign key)
            title
            img_url
            content
relasi  :    
--------------------------------------------  