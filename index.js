const {Client} = require('pg');

class MyDB{

    #email = 'admin@gmail.com'
    #password = 'rahasia'
    #databaseName = 'social_media';

    constructor(email,password){
        this.isAuthenticated = this.#authentication(email,password)
        this.client = new Client({
            user : 'postgres',
            host: 'localhost',
            password: 'rahasia',
            port : 5432
        })
    }

    #authentication(email,password){
        return this.#email === email && this.#password === password
    }

    async createDatabase(databaseName){
        this.#databaseName = databaseName
        if(this.isAuthenticated){
            const query = `CREATE DATABASE ${databaseName}`;
        
            this.client.connect();
    
            try{
                const res = await this.client.query(query);
                if(res) console.log('Database Berhasil Dibuat');
            }catch(err){
                console.log('Database gagal dibuat ');
            }finally{
                this.client.end()
            }
            return true;
        }
        console.log('Anda tidak punya akses');
    }

    #useDB(){
        this.client =  new Client({
            user : 'postgres',
            host: 'localhost',
            password: 'rahasia',
            database: this.#databaseName,
            port : 5432
        })
    }

    async createAccountTable(){
        if(this.isAuthenticated){
            this.#useDB();
            const query = `CREATE TABLE account (
                account_id serial PRIMARY KEY,
                username varchar(100) UNIQUE NOT NULL,
                password varchar(100) NOT NULL
            )`;
        
            this.client.connect();
    
            try{
                const res = await this.client.query(query);
                if(res) console.log('Tabel Berhasil Dibuat');
            }catch(err){
                console.log('Tabel gagal dibuat');
            }finally{
                this.client.end()
            }

            return true;
        }
        console.log('Anda tidak punya akses');
    }

    async createProfileTable(){
        if(this.isAuthenticated){
            this.#useDB();
            const query = `CREATE TABLE profile (
                profile_id serial PRIMARY KEY,
                username varchar(100) UNIQUE NOT NULL REFERENCES account(username) ON DELETE CASCADE,
                nickname varchar(100) NOT NULL,
                description varchar(255) NOT NULL
            )`;
        
            this.client.connect();
    
            try{
                const res = await this.client.query(query);
                if(res) console.log('Tabel Berhasil Dibuat');
            }catch(err){
                console.log('Tabel gagal dibuat');
            }finally{
                this.client.end()
            }

            return true;
        }
        console.log('Anda tidak punya akses');
    }

    async createPostTable(){
        if(this.isAuthenticated){
            this.#useDB();
            const query = `CREATE TABLE post (
                post_id serial PRIMARY KEY,
                username varchar(100) NOT NULL REFERENCES account(username) ON DELETE CASCADE,
                title varchar(255),
                img_url text,
                content text NOT NULL
            )`;
        
            this.client.connect();
    
            try{
                const res = await this.client.query(query);
                if(res) console.log('Tabel Berhasil Dibuat');
            }catch(err){
                console.log('Tabel gagal dibuat');
            }finally{
                this.client.end()
            }

            return true;
        }
        console.log('Anda tidak punya akses');
    }


}

class User{

    #client = new Client({
        user : 'postgres',
        host: 'localhost',
        password: 'rahasia',
        database: 'social_media',
        port : 5432
    })

    constructor(userObj){
        const username = userObj.username;
        const password = userObj.password; 
        this.username = username;
        this.password = this.#simpleHashFunction(password);
    }

    async createAccount(){

        const query = `INSERT INTO account (username, password ) VALUES ('${this.username}', '${this.password}')`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Account berhasil dibuat');
        }catch(err){
            console.log('Account gagal dibuat', err);
        }finally{
            this.#client.end()
        }

    }

    async resetPassword(newPassword){
        const query = `UPDATE account SET password = '${this.#simpleHashFunction(newPassword)}' WHERE username = '${this.username}'`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Password berhasil diubah');
        }catch(err){
            console.log('Password gagal diubah', err);
        }finally{
            this.#client.end()
        }
    }

    async createProfile(profileObj){
        const nickname = profileObj.nickname;
        const description = profileObj.description;

        const query = `INSERT INTO profile (username, nickname, description ) VALUES ('${this.username}', '${nickname}','${description}')`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Profil berhasil dibuat');
        }catch(err){
            console.log('Profil gagal dibuat', err);
        }finally{
            this.#client.end()
        }

    }

    async updateProfile(profileObj){
        const nickname = profileObj.nickname;
        const description = profileObj.description;

        const query = `UPDATE profile SET nickname = '${nickname}', description = '${description}' WHERE username = '${this.username}'`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Profil berhasil diubah');
        }catch(err){
            console.log('Profil gagal diubah', err);
        }finally{
            this.#client.end()
        }

    }

    async getProfile(){

        const query = `select profile.nickname as nickname, profile.description as description, count(post.username) as jumlah_post 
        from profile left join post 
        on profile.username=post.username
        where profile.username = '${this.username}'
        group by profile.nickname, profile.description`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log(res.rows[0]);
        }catch(err){
            console.log('Profil tidak ditemukan', err);
        }finally{
            this.#client.end()
        }
    }

    async createPost(postObj){
        const {title,img_url,content} = postObj;

        const query = `INSERT INTO post 
        (username,title,img_url,content) 
        VALUES
        ('${this.username}','${title}','${img_url}','${content}') 
        `;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Post berhasil dibuat');
        }catch(err){
            console.log('Post gagal dibuat', err);
        }finally{
            this.#client.end()
        }

    }

    async getPostByUsername(username){
        const query = `SELECT title,img_url,content FROM post WHERE username = '${username}'`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log(res.rows);
        }catch(err){
            console.log('Post gagal dibuat', err);
        }finally{
            this.#client.end()
        }
    }

    async updatePost(id,props){
        const {title,img_url,content} = props;
        const query = `UPDATE post SET 
        title='${title}', img_url = '${img_url}', content = '${content}'
        WHERE post_id = '${id}'`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Post berhasil di update');
        }catch(err){
            console.log('Post gagal di update', err);
        }finally{
            this.#client.end()
        }   
    }

    async deletePost(id){
        const query = `DELETE FROM post WHERE post_id = '${id}'`;

        this.#client.connect();

        try{
            const res = await this.#client.query(query);
            if(res) console.log('Post berhasil di delete');
        }catch(err){
            console.log('Post gagal di delete', err);
        }finally{
            this.#client.end()
        }   
    }

    #simpleHashFunction(password){
        return password + '123'
    }
}

const admin = new MyDB('admin@gmail.com','rahasia')
// JALANKAN SATU SATU 

// membuat database dengan parameter berupa nama database yang akan dibuat
// admin.createDatabase('social_media')

// membuat tabel account (tidak perlu input parameter)
// admin.createAccountTable()

// membuat tabel profile (tidak perlu input parameter)
// admin.createProfileTable()

// membuat tabel post (tidak perlu input parameter)
// admin.createPostTable()

const ozza = new User({username:'ozza',password:'rahasia'})

// membuat akun (tanpa parameter)
// ozza.createAccount()

// merubah password (parameternya berupa string yang berisi password baru)
// ozza.resetPassword('abc')

// membuat profil (parameternya berupa objek yang berisi nickname dan description)
// ozza.createProfile({nickname:'test',description:'desc'})


// merubah profil (parameternya berupa objek yang berisi nickname dan description)
// ozza.updateProfile({nickname:'test',description:'desc updated'})

// mengambil profil (tanpa parameter)
// ozza.getProfile()

// membuat post(parameternya berupa objek yang berisi title, img_url(boleh kosong), content)
// ozza.createPost({title:'judul 1',img_url:'img url 1', content:'content 1'})

// ozza.createPost({title:'judul 3',img_url:'img url 3', content:'content 3'})

// mengambil pos berdasarkan username
// ozza.getPostByUsername('ozza');

// merubah post (parameternya berupa id post, dan objek yang berisi titile, img_url (boleh kosong), dan content)
// ozza.updatePost(1,{title:'judul 1 updated',img_url:'img url 1 updated', content:'content 1 updated'})

// menghapus post(parameternya berupa id post)
// ozza.deletePost(1)


const ozza2 = new User({username:'ozza2',password:'rahasia2'})
// ozza2.createAccount()

// ozza2.createProfile({nickname:'test2',description:'desc 2'})

// ozza2.createPost({title:'judul 1',img_url:'img url 1', content:'content 1'})

// ozza2.createPost({title:'judul 3',img_url:'img url 3', content:'content 3'})

// ozza.getProfile()

